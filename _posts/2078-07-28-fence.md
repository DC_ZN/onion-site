---
layout: post
title: Операция "Забор"
categories: 
- attack
author: Чиполлино

---

> Пиши долг на забор, забор упадет, и долг пропадет.

### Хронология
Слон обнаружил уязвимость в системе Ётериума, однако, её не так-то просто проэксплуатировать.
Нам потребовался доступ глубоко внутрь Ё-корп. И он был найден -- на одном из подпольных форумов мы нашли утечку инструментов APT30 "CavalryBear". В одном из них упоминался адрес в Торе, который, как оказалось, хостился внутри Ё-корп. 

![APT30](/images/cavalry_bear.jpg)

Я проанализировал траффик их агента и обнаружил уязвимость в С&C-сервисе, проэксплуатировал её и в итоге получил контроль над этим хостом.
Наш доблестный админ установил туда ssh для большего удобства.

После этого вошёл Слон и буквально провёл нам мастер-класс продвижения во внутренней сети. Всё складывалось очень хорошо, и через несколько дней мы получили объект, хоть и в зашифрованном виде.

Теперь осталось только найти пароль или ключ, которым он зашифрован.

### Текущие задачи

* Провести дальнейшую атаку для того, чтобы расшифровать артефакт

__Чиполлино__
